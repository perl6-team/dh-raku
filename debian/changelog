dh-raku (0.17) unstable; urgency=medium

  * dh_raku_test: use only Perl's prove
  * control: add dependency on perl

 -- Dominique Dumont <dod@debian.org>  Sat, 19 Oct 2024 12:27:31 +0200

dh-raku (0.16) unstable; urgency=medium

  * cleanup precompiled files with dh_clean.
  * rules: install dh_raku_clean man page

 -- Dominique Dumont <dod@debian.org>  Sun, 15 Oct 2023 18:08:57 +0200

dh-raku (0.15) unstable; urgency=medium

  * dh_raku_build: add rdepends on raku-api-<compiler-id>

 -- Dominique Dumont <dod@debian.org>  Wed, 18 Jan 2023 09:10:22 +0100

dh-raku (0.14) unstable; urgency=medium

  * Revert "dh_raku_build: remove $HOME setup" (Closes: 1020788)

 -- Dominique Dumont <dod@debian.org>  Tue, 27 Sep 2022 12:15:32 +0200

dh-raku (0.13) unstable; urgency=medium

  * dh_raku_build:
    * set RAKULIB variable to debian tmp dir (Closes: 1019579)
    * remove $HOME setup
    * set RAKUDO_MODULE_DEBUG to 1

 -- Dominique Dumont <dod@debian.org>  Sat, 24 Sep 2022 18:16:51 +0200

dh-raku (0.12) unstable; urgency=medium

  * dh_raku_test: fix typo when checking prove6 binary
  * control: declare compliance with policy 4.6.1

 -- Dominique Dumont <dod@debian.org>  Sun, 04 Sep 2022 16:50:20 +0200

dh-raku (0.11) unstable; urgency=medium

  * dh_raku_test: use -Ilib option when running raku with perl5 prove
    (Closes: 1015079)

 -- Dominique Dumont <dod@debian.org>  Sun, 17 Jul 2022 17:16:20 +0200

dh-raku (0.10) unstable; urgency=medium

  * fix dh_raku_test bug: do not run prove6 when it's missing

 -- Dominique Dumont <dod@debian.org>  Mon, 27 Jun 2022 12:39:16 +0200

dh-raku (0.9) unstable; urgency=medium

  * dh_raku_build:
    * make precomp phase more verbose
    * use install-dist.raku when possible
    * show install command
  * dh_raku_test:
    * remove obsolete doc
    * can use prove when prove6 is not found

 -- Dominique Dumont <dod@debian.org>  Sun, 26 Jun 2022 15:27:08 +0200

dh-raku (0.8) unstable; urgency=medium

  * dh_raku_build: set HOME directory to debian/tmp/home

 -- Dominique Dumont <dod@debian.org>  Mon, 20 Jun 2022 11:14:22 +0200

dh-raku (0.7) unstable; urgency=medium

  * fix dh_raku_install doc.
    Thanks to gregoa for the heads-up (Closes: 1003199)
  * dh-raku: sort the list of installed files.
    Thanks to Chris Lamb for the patch (Closes: 1003159)
  * dh_raku_install: reduce verbosity
  * dh_raku_test: set HOME for tests
  * dh_raku_build: set HOME to a non existent directory

 -- Dominique Dumont <dod@debian.org>  Sun, 20 Feb 2022 18:40:00 +0100

dh-raku (0.6) unstable; urgency=medium

  * control: add rakudo dependency

 -- Dominique Dumont <dod@debian.org>  Sat, 08 Jan 2022 15:13:59 +0100

dh-raku (0.5) unstable; urgency=medium

  * set /usr/bin/raku as #! interpreter

 -- Dominique Dumont <dod@debian.org>  Wed, 29 Dec 2021 16:36:13 +0100

dh-raku (0.4) unstable; urgency=medium

  * dh-raku: handle installation of Raku scripts in /usr/bin

 -- Dominique Dumont <dod@debian.org>  Tue, 28 Dec 2021 18:53:06 +0100

dh-raku (0.3) unstable; urgency=medium

  * dh_raku_install:
    * skip empty directories
    * install a list of pre-compiled files
  * dh_raku_build:
    * inject dependency on raku-api version
    * fix documentation

 -- Dominique Dumont <dod@debian.org>  Wed, 15 Dec 2021 18:29:19 +0100

dh-raku (0.2) unstable; urgency=medium

  * Update README.org
  * install README.org
  * control: provides dh-sequence-raku (Closes: #1000624)
    Thanks gregoa for the heads-up
  * fix (doc): suggest to build-depend on dh-sequence-raku
  * control: no need for root in rules
  * rules: provide dh_raku_install man page

 -- Dominique Dumont <dod@debian.org>  Tue, 30 Nov 2021 18:32:25 +0100

dh-raku (0.1) unstable; urgency=medium

  * Initial release

 -- Dominique Dumont <dod@debian.org>  Sun, 14 Nov 2021 15:21:36 +0100
