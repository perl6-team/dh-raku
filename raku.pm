use warnings;
use strict;

use Debian::Debhelper::Dh_Lib;

# add build instructions
insert_after("dh_auto_build", "dh_raku_build");
remove_command("dh_auto_build");

# install files
insert_before("dh_auto_install", "dh_raku_install");
remove_command("dh_auto_install");

# automatically run the test suite
insert_after("dh_auto_test", "dh_raku_test");
remove_command("dh_auto_test");

# cleanup precompiled files
insert_after("dh_clean", "dh_raku_clean");

remove_command("dh_prep");

1;
